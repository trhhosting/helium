class EventRequest {
  String _email;
  String _appName;
  String _emailHash;
  Client _client;
  Event _event;
  String _calendarId;
  int _amountPaid;
  int _dayRange;
  String _page;

  EventRequest(
      {
        String email,
        String appName,
        Client client,
        Event event,
        String calendarId,
        int amountPaid,
        int dayRange,
        String page,
        String emailHash,
      }) 
      {
        this._email = email;
        this._appName = appName;
        this._client = client;
        this._event = event;
        this._calendarId = calendarId;
        this._amountPaid = amountPaid;
        this._dayRange = dayRange;
        this._page = page;
        this._emailHash = emailHash;
      }

  String get email => _email;
  set email(String email) => _email = email;
  String get appName => _appName;
  set appName(String appName) => _appName = appName;
  Client get client => _client;
  set client(Client client) => _client = client;
  Event get event => _event;
  set event(Event event) => _event = event;
  String get calendarId => _calendarId;
  set calendarId(String calendarId) => _calendarId = calendarId;
  int get amountPaid => _amountPaid;
  set amountPaid(int amountPaid) => _amountPaid = amountPaid;
  int get dayRange => _dayRange;
  set dayRange(int dayRange) => _dayRange = dayRange;
  //page Page uri
  String get page => this._page;
  set page(String page){
  this._page = page;
  }
  //emailHash Desc
  String get emailHash => this._emailHash;
  set emailHash(String emailHash){
  this._emailHash = emailHash;
  }
  EventRequest.fromJson(Map<String, dynamic> json) {
    _email = json['email'];
    _appName = json['app_name'];
    _client =
        json['Client'] != null ? new Client.fromJson(json['Client']) : null;
    _event = json['event'] != null ? new Event.fromJson(json['event']) : null;
    _calendarId = json['calendar_id'];
    _amountPaid = json['amount_paid'];
    _dayRange = json['day_range'];
    _page = json['page'] as String;
    _emailHash = json['emai_hash'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = this._email;
    data['app_name'] = this._appName;
    if (this._client != null) {
      data['Client'] = this._client.toJson();
    }
    if (this._event != null) {
      data['event'] = this._event.toJson();
    }
    data['calendar_id'] = this._calendarId;
    data['amount_paid'] = this._amountPaid;
    data['day_range'] = this._dayRange;
    data['page'] = this._page;
    data['email_hash'] = this._emailHash;
    return data;
  }
}

class Client {
  String _email;
  String _phoneNumber;
  String _name;

  Client({String email, String phoneNumber, String name}) {
    this._email = email;
    this._phoneNumber = phoneNumber;
    this._name = name;
  }

  String get email => _email;
  set email(String email) => _email = email;
  String get phoneNumber => _phoneNumber;
  set phoneNumber(String phoneNumber) => _phoneNumber = phoneNumber;
  String get name => _name;
  set name(String name) => _name = name;

  Client.fromJson(Map<String, dynamic> json) {
    _email = json['email'];
    _phoneNumber = json['phone_number'];
    _name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = this._email;
    data['phone_number'] = this._phoneNumber;
    data['name'] = this._name;
    return data;
  }
}

class Event {
  Attendees endees;
  String _description;
  Start _start;
  Start _end;
  String _eventId;
  Location _location;
  List<Reminders> _reminders;
  String _summary;
  String _url;

  Event(
      {
        Attendees attendees,
        String description,
        Start start,
        Start end,
        String eventId,
        Location location,
        List<Reminders> reminders,
        String summary,
        String url
      }) {
        this.endees = attendees;
        this._description = description;
        this._start = start;
        this._end = end;
        this._eventId = eventId;
        this._location = location;
        this._reminders = reminders;
        this._summary = summary;
        this._url = url;
  }

  Attendees get attendees => endees;
  set attendees(Attendees attendees) => endees = attendees;
  String get description => _description;
  set description(String description) => _description = description;
  Start get start => _start;
  set start(Start start) => _start = start;
  Start get end => _end;
  set end(Start end) => _end = end;
  String get eventId => _eventId;
  set eventId(String eventId) => _eventId = eventId;
  Location get location => _location;
  set location(Location location) => _location = location;
  List<Reminders> get reminders => _reminders;
  set reminders(List<Reminders> reminders) => _reminders = reminders;
  String get summary => _summary;
  set summary(String summary) => _summary = summary;
  String get url => _url;
  set url(String url) => _url = url;

  Event.fromJson(Map<String, dynamic> json) {
    endees = json['attendees'] != null
        ? new Attendees.fromJson(json['attendees'])
        : null;
    _description = json['description'];
    _start = json['start'] != null ? new Start.fromJson(json['start']) : null;
    _end = json['end'] != null ? new Start.fromJson(json['end']) : null;
    _eventId = json['event_id'];
    _location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
    if (json['reminders'] != null) {
      _reminders = <Reminders>[];
      json['reminders'].forEach((v) {
        _reminders.add(new Reminders.fromJson(v));
      });
    }
    _summary = json['summary'];
    _url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.endees != null) {
      data['attendees'] = this.endees.toJson();
    }
    data['description'] = this._description;
    if (this._start != null) {
      data['start'] = this._start.toJson();
    }
    if (this._end != null) {
      data['end'] = this._end.toJson();
    }
    data['event_id'] = this._eventId;
    if (this._location != null) {
      data['location'] = this._location.toJson();
    }
    if (this._reminders != null) {
      data['reminders'] = this._reminders.map((v) => v.toJson()).toList();
    }
    data['summary'] = this._summary;
    data['url'] = this._url;
    return data;
  }
}

class Attendees {
  List<InviteAtt> _inviteAtt;
  List<InviteAtt> _removeAtt;

  Attendees({List<InviteAtt> inviteAtt, List<InviteAtt> removeAtt}) {
    this._inviteAtt = inviteAtt;
    this._removeAtt = removeAtt;
  }

  List<InviteAtt> get inviteAtt => _inviteAtt;
  set inviteAtt(List<InviteAtt> inviteAtt) => _inviteAtt = inviteAtt;
  List<InviteAtt> get removeAtt => _removeAtt;
  set removeAtt(List<InviteAtt> removeAtt) => _removeAtt = removeAtt;

  Attendees.fromJson(Map<String, dynamic> json) {
    if (json['invite'] != null) {
      _inviteAtt = <InviteAtt>[];
      json['invite'].forEach((v) {
        _inviteAtt.add(new InviteAtt.fromJson(v));
      });
    }
    if (json['remove'] != null) {
      _removeAtt = <InviteAtt>[];
      json['remove'].forEach((v) {
        _removeAtt.add(new InviteAtt.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this._inviteAtt != null) {
      data['invite'] = this._inviteAtt.map((v) => v.toJson()).toList();
    }
    if (this._removeAtt != null) {
      data['remove'] = this._removeAtt.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InviteAtt {
  String _displayName;
  String _email;

  InviteAtt({String displayName, String email}) {
    this._displayName = displayName;
    this._email = email;
  }

  String get displayName => _displayName;
  set displayName(String displayName) => _displayName = displayName;
  String get email => _email;
  set email(String email) => _email = email;

  InviteAtt.fromJson(Map<String, dynamic> json) {
    _displayName = json['display_name'];
    _email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['display_name'] = this._displayName;
    data['email'] = this._email;
    return data;
  }
}

class Start {
  String _time;
  String _tzid;

  Start({String time, String tzid}) {
    this._time = time;
    this._tzid = tzid;
  }

  String get time => _time;
  set time(String time) => _time = time;
  String get tzid => _tzid;
  set tzid(String tzid) => _tzid = tzid;

  Start.fromJson(Map<String, dynamic> json) {
    _time = json['time'];
    _tzid = json['tzid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['time'] = this._time;
    data['tzid'] = this._tzid;
    return data;
  }
}

class Location {
  String _description;
  String _lat;
  String _long;

  Location({String description, String lat, String long}) {
    this._description = description;
    this._lat = lat;
    this._long = long;
  }

  String get description => _description;
  set description(String description) => _description = description;
  String get lat => _lat;
  set lat(String lat) => _lat = lat;
  String get long => _long;
  set long(String long) => _long = long;

  Location.fromJson(Map<String, dynamic> json) {
    _description = json['description'];
    _lat = json['lat'];
    _long = json['long'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['description'] = this._description;
    data['lat'] = this._lat;
    data['long'] = this._long;
    return data;
  }
}

class Reminders {
  int _minutes;

  Reminders({int minutes}) {
    this._minutes = minutes;
  }

  int get minutes => _minutes;
  set minutes(int minutes) => _minutes = minutes;

  Reminders.fromJson(Map<String, dynamic> json) {
    _minutes = json['minutes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['minutes'] = this._minutes;
    return data;
  }
}
