class RequestAppointment {
  String _appname;
  TimeBlock _timeBlock;
  String _hash;
  String _clientName;
  String _clientEmail;
  String _clientNumber;

  RequestAppointment(
      {String appname,
      TimeBlock timeBlock,
      String hash,
      String clientName,
      String clientEmail,
      String clientNumber}) {
    this._appname = appname;
    this._timeBlock = timeBlock;
    this._hash = hash;
    this._clientName = clientName;
    this._clientEmail = clientEmail;
    this._clientNumber = clientNumber;
  }

  String get appname => _appname;
  set appname(String appname) => _appname = appname;
  TimeBlock get timeBlock => _timeBlock;
  set timeBlock(TimeBlock timeBlock) => _timeBlock = timeBlock;
  String get hash => _hash;
  set hash(String hash) => _hash = hash;
  String get clientName => _clientName;
  set clientName(String clientName) => _clientName = clientName;
  String get clientEmail => _clientEmail;
  set clientEmail(String clientEmail) => _clientEmail = clientEmail;
  String get clientNumber => _clientNumber;
  set clientNumber(String clientNumber) => _clientNumber = clientNumber;

  RequestAppointment.fromJson(Map<String, dynamic> json) {
    _appname = json['appname'];
    _timeBlock = json['time_block'] != null
        ? new TimeBlock.fromJson(json['time_block'])
        : null;
    _hash = json['hash'];
    _clientName = json['client_name'];
    _clientEmail = json['client_email'];
    _clientNumber = json['client_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['appname'] = this._appname;
    if (this._timeBlock != null) {
      data['time_block'] = this._timeBlock.toJson();
    }
    data['hash'] = this._hash;
    data['client_name'] = this._clientName;
    data['client_email'] = this._clientEmail;
    data['client_number'] = this._clientNumber;
    return data;
  }
}

class TimeBlock {
  int _before;
  int _after;
  int _duration;
  String _dateTime;
  String _timeZone;
  String _timeOpen;
  String _timeClose;

  TimeBlock(
      {int before,
      int after,
      int duration,
      String dateTime,
      String timeZone,
      String timeOpen,
      String timeClose}) {
    this._before = before;
    this._after = after;
    this._duration = duration;
    this._dateTime = dateTime;
    this._timeZone = timeZone;
    this._timeOpen = timeOpen;
    this._timeClose = timeClose;
  }

  int get before => _before;
  set before(int before) => _before = before;
  int get after => _after;
  set after(int after) => _after = after;
  int get duration => _duration;
  set duration(int duration) => _duration = duration;
  String get dateTime => _dateTime;
  set dateTime(String dateTime) => _dateTime = dateTime;
  String get timeZone => _timeZone;
  set timeZone(String timeZone) => _timeZone = timeZone;
  String get timeOpen => _timeOpen;
  set timeOpen(String timeOpen) => _timeOpen = timeOpen;
  String get timeClose => _timeClose;
  set timeClose(String timeClose) => _timeClose = timeClose;

  TimeBlock.fromJson(Map<String, dynamic> json) {
    _before = json['before'];
    _after = json['after'];
    _duration = json['duration'];
    _dateTime = json['date_time'];
    _timeZone = json['time_zone'];
    _timeOpen = json['time_open'];
    _timeClose = json['time_close'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['before'] = this._before;
    data['after'] = this._after;
    data['duration'] = this._duration;
    data['date_time'] = this._dateTime;
    data['time_zone'] = this._timeZone;
    data['time_open'] = this._timeOpen;
    data['time_close'] = this._timeClose;
    return data;
  }
}
