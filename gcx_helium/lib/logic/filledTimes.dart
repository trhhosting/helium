class FilledTimes {
  List<BlockedTimes> _blockedTimes;
  SelectedRange _selectedRange;
  int _numberDays;
  String _dateSelected;

  FilledTimes(
      {List<BlockedTimes> blockedTimes,
      SelectedRange selectedRange,
      int numberDays,
      String dateSelected}) {
    this._blockedTimes = blockedTimes;
    this._selectedRange = selectedRange;
    this._numberDays = numberDays;
    this._dateSelected = dateSelected;
  }

  List<BlockedTimes> get blockedTimes => _blockedTimes;
  set blockedTimes(List<BlockedTimes> blockedTimes) =>
      _blockedTimes = blockedTimes;
  SelectedRange get selectedRange => _selectedRange;
  set selectedRange(SelectedRange selectedRange) =>
      _selectedRange = selectedRange;
  int get numberDays => _numberDays;
  set numberDays(int numberDays) => _numberDays = numberDays;
  String get dateSelected => _dateSelected;
  set dateSelected(String dateSelected) => _dateSelected = dateSelected;

  FilledTimes.fromJson(Map<String, dynamic> json) {
    if (json['blocked_times'] != null) {
      _blockedTimes = <BlockedTimes>[];
      json['blocked_times'].forEach((v) {
        _blockedTimes.add(new BlockedTimes.fromJson(v));
      });
    }
    _selectedRange = json['selected_range'] != null
        ? new SelectedRange.fromJson(json['selected_range'])
        : null;
    _numberDays = json['number_days'];
    _dateSelected = json['date_selected'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this._blockedTimes != null) {
      data['blocked_times'] =
          this._blockedTimes.map((v) => v.toJson()).toList();
    }
    if (this._selectedRange != null) {
      data['selected_range'] = this._selectedRange.toJson();
    }
    data['number_days'] = this._numberDays;
    data['date_selected'] = this._dateSelected;
    return data;
  }
}

class BlockedTimes {
  String _id;
  String _startDate;
  String _endDate;
  String _startTime;
  String _endTime;
  String _calendarId;

  BlockedTimes(
      {
      String id,
      String startDate,
      String endDate,
      String startTime,
      String endTime,
      String calendarId
      }) {
    this._id = id;
    this._startDate = startDate;
    this._endDate = endDate;
    this._startTime = startTime;
    this._endTime = endTime;
    this._calendarId = calendarId;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get startDate => _startDate;
  set startDate(String startDate) => _startDate = startDate;
  String get endDate => _endDate;
  set endDate(String endDate) => _endDate = endDate;
  String get startTime => _startTime;
  set startTime(String startTime) => _startTime = startTime;
  String get endTime => _endTime;
  set endTime(String endTime) => _endTime = endTime;

  BlockedTimes.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _startDate = json['start_date'];
    _endDate = json['end_date'];
    _startTime = json['start_time'];
    _endTime = json['end_time'];
    _calendarId = json['calendar_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = this._id;
    data['start_date'] = this._startDate;
    data['end_date'] = this._endDate;
    data['start_time'] = this._startTime;
    data['end_time'] = this._endTime;
    data['calendar_id'] = this._calendarId;
    return data;
  }
}

class SelectedRange {
  String _fromDate;
  String _toDate;

  SelectedRange({String fromDate, String toDate}) {
    this._fromDate = fromDate;
    this._toDate = toDate;
  }

  String get fromDate => _fromDate;
  set fromDate(String fromDate) => _fromDate = fromDate;
  String get toDate => _toDate;
  set toDate(String toDate) => _toDate = toDate;

  SelectedRange.fromJson(Map<String, dynamic> json) {
    _fromDate = json['from_date'];
    _toDate = json['to_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['from_date'] = this._fromDate;
    data['to_date'] = this._toDate;
    return data;
  }
}
