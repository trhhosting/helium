class Items {
  String desc;
  String name;
  String id;
  int price;

  Items({this.desc, this.name, this.id, this.price});

  Items.fromJson(Map<String, dynamic> json) {
    desc = json['desc'];
    name = json['name'];
    id = json['id'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['desc'] = this.desc;
    data['name'] = this.name;
    data['id'] = this.id;
    data['price'] = this.price;
    return data;
  }
}
