class KayDeeServices {
  String category;
  String colorCode;
  String description;
  int duration;
  Images images;
  String message;
  String name;
  int price;
  TimeBlock timeBlock;

  KayDeeServices(
      {this.category,
      this.colorCode,
      this.description,
      this.duration,
      this.images,
      this.message,
      this.name,
      this.price = 0,
      this.timeBlock});

  KayDeeServices.fromJson(Map<String, dynamic> json) {
    category = json['category'];
    colorCode = json['color_code'];
    description = json['description'];
    duration = json['duration'];
    images =
        json['images'] != null ? new Images.fromJson(json['images']) : null;
    message = json['message'];
    name = json['name'];
    price = json['price'];
    timeBlock = json['time_block'] != null
        ? new TimeBlock.fromJson(json['time_block'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['category'] = this.category;
    data['color_code'] = this.colorCode;
    data['description'] = this.description;
    data['duration'] = this.duration;
    if (this.images != null) {
      data['images'] = this.images.toJson();
    }
    data['message'] = this.message;
    data['name'] = this.name;
    data['price'] = this.price;
    if (this.timeBlock != null) {
      data['time_block'] = this.timeBlock.toJson();
    }
    return data;
  }
}

class Images {
  String description;
  String location;
  String title;

  Images({this.description, this.location, this.title});

  Images.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    location = json['location'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['description'] = this.description;
    data['location'] = this.location;
    data['title'] = this.title;
    return data;
  }
}

class TimeBlock {
  int after;
  int before;

  TimeBlock({this.after, this.before});

  TimeBlock.fromJson(Map<String, dynamic> json) {
    after = json['after'];
    before = json['before'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['after'] = this.after;
    data['before'] = this.before;
    return data;
  }
}
