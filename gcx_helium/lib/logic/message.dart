class Message {
  String _title;
  String _subTitle;
  String _message;

  Message({String title, String subTitle, String message}) {
    this._title = title;
    this._subTitle = subTitle;
    this._message = message;
  }

  String get title => _title;
  set title(String title) => _title = title;
  String get subTitle => _subTitle;
  set subTitle(String subTitle) => _subTitle = subTitle;
  String get message => _message;
  set message(String message) => _message = message;

  Message.fromJson(Map<String, dynamic> json) {
    _title = json['title'];
    _subTitle = json['sub_title'];
    _message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = this._title;
    data['sub_title'] = this._subTitle;
    data['message'] = this._message;
    return data;
  }
}
