
class TimeCompair{
    int _hour;
    int _min;
    int _slotLenth;
    TimeCompair();
    TimeCompair.Manual(int hour, int min, [int slot = 30]) {
      this._hour = hour;
      this._min = hour;
      this._slotLenth = slot;
    }
    TimeCompair.fromTimeSlotString(String timeslot) {
      this._hour = textToHour(timeslot);
      this._min = textToMin(timeslot);
    }
    TimeCompair.fromDateTime(DateTime date) {
      this._hour = date.hour;
      this._min = date.minute;
    }
    TimeCompair.fromMinuntes(int span) {
      double tmp = span / 60;
      List<String> t = tmp.toString().split(".");
      // print("TimeCompair ${t[0]}");
      // print("TimeCompair ${t[1]}");
      if(t.length == 2){
      this._hour = int.parse(t[0]);
      // this._min = _roundMin((double.parse(".${t[1]}") * 60).toInt());
      this._min = (double.parse(".${t[1]}") * 60).toInt();
      }
    }

    //hour hour
    int get hour => this._hour;
    set hour(int hour){
    this._hour = hour;
    }

    //min min
    int get min => this._min;
    set min(int min){
    this._min = min;
    } 
    //slotLenth This time between hours displayed
    int get slotLenth => this._slotLenth;
    set slotLenth(int slotLenth){
    this._slotLenth = slotLenth;
    }

  int textToHour(String time){
    String hr = time.split(":")[0];
    return int.parse(hr);
  }
  int textToMin(String time){
    String mm = time.split(":")[1];
    return int.parse(mm);
  }
  int textToYear(String time){
    String mm = time.split("-")[0];
    return int.parse(mm);
  } 
  int textToMonth(String time){
    String mm = time.split("-")[1];
    return int.parse(mm);
  } 
  int textToDay(String time){
    String mm = time.split("-")[2];
    mm = mm.split("T")[0];
    return int.parse(mm);
  } 
  }

