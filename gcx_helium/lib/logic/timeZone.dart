class TimeZone {
  String _value;
  String _abbr;
  int _offset;
  bool _isdst;
  String _text;
  List<String> _utc;

  TimeZone(
      {String value,
      String abbr,
      int offset,
      bool isdst,
      String text,
      List<String> utc}) {
    this._value = value;
    this._abbr = abbr;
    this._offset = offset;
    this._isdst = isdst;
    this._text = text;
    this._utc = utc;
  }

  String get value => _value;
  set value(String value) => _value = value;
  String get abbr => _abbr;
  set abbr(String abbr) => _abbr = abbr;
  int get offset => _offset;
  set offset(int offset) => _offset = offset;
  bool get isdst => _isdst;
  set isdst(bool isdst) => _isdst = isdst;
  String get text => _text;
  set text(String text) => _text = text;
  List<String> get utc => _utc;
  set utc(List<String> utc) => _utc = utc;

  TimeZone.fromJson(Map<String, dynamic> json) {
    _value = json['value'];
    _abbr = json['abbr'];
    _offset = json['offset'];
    _isdst = json['isdst'];
    _text = json['text'];
    _utc = json['utc'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['value'] = this._value;
    data['abbr'] = this._abbr;
    data['offset'] = this._offset;
    data['isdst'] = this._isdst;
    data['text'] = this._text;
    data['utc'] = this._utc;
    return data;
  }
}
