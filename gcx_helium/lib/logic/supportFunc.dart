import 'package:logic/logic.dart';
import 'dart:html';



Future<String> loadServices() async {
    String serviesData = "";
     var i = 0;
      do {
        if (i > 50){
          break;
        }
        i++;
        serviesData = await getData('services');
        print("loading $i");
      } while (serviesData == null);
      
    return serviesData;
  }
Future<String> loadAppSettings() async {
    String tmp = "1";
     var i = 0;
      do {
        tmp = await getData('app');
        if(tmp != null && tmp != "1"){
          break;
        }
        i++;
        print("loading AppSettings $i");
      } while (tmp == "1" );
      
    return tmp;
  }
 void top(){
    window.scrollTo(0,0);
  }
  /*
      var i = 0;
      do {
        if (i > 50){
          break;
        }
        i++;
        serviesData = await getData('services');
        print("loading $i");
      } while (serviesData == null);
  */

  String iso8601ToRFC3339(DateTime date){
    // 2020-11-15T11:20:00Z
    date = date.toUtc();
    // String dayText, hourText, minText = "";
    // if(date.day < 10){
    //   dayText = "0${date.day}";
    // } else {
    //   dayText = "${date.day}";      
    // }
    // if(date.hour < 10){
    //   hourText = "0${date.hour}";
    // } else {
    //   hourText = "${date.hour}";      
    // }
    // if(date.minute < 10){
    //   minText = "0${date.minute}";
    // } else {
    //    minText = "${date.minute}";
    // }
    // print("iso8601ToRFC3339: ${dayText}");
    // print("iso8601ToRFC3339: ${hourText}");
    String tmz = DateTime.now().timeZoneOffset.toString().split(":")[0];
    // 00Z
    int tmzT = int.parse(tmz);
    // String tm = "";
    if(tmzT < 10 && tmzT > 0){
      tmz  = "0${tmzT}";
    } else if (tmzT > 1) {
       tmz = "${tmzT}";
    }
    if(tmzT < 0 ){
      tmz = tmz * -1;
      tmz  = "${tmzT}";
    } else if (tmzT < -10) {
      tmz = tmz * -1;
       tmz = "-${tmzT}";
    }


    // return "${date.year}-${date.month}-${dayText}T${hourText}:${minText}:${tmz}Z";
    return date.toIso8601String();
  }

  Map<String, int> WeekDayNameNo = {"Monday":1, "Tuesday":2, "Wednesday":3, "Thursday":4, "Friday":5, "Saturday":6, "Sunday":6};

  // 2020-11-25T08:57:00Z
  int textToHour(String time){
    String hr = time.split(":")[0];
    return int.parse(hr);
  }
  int textToMin(String time){
    String mm = time.split(":")[1];
    return int.parse(mm);
  }
  int textToYear(String time){
    String mm = time.split("-")[0];
    return int.parse(mm);
  } 
  int textToMonth(String time){
    String mm = time.split("-")[1];
    return int.parse(mm);
  } 
  int textToDay(String time){
    String mm = time.split("-")[2];
    mm = mm.split("T")[0];
    return int.parse(mm);
  } 

  List<String> timeSlots(String open, String close, [int slotLenth = 30]){
    List<String> slots = <String>[];
    int openHour = textToHour(open);
    int closeHour = textToHour(close);
    int count = 0;
    do{
      if(count == 0){
        count = openHour;
      }
      for (var i = 0; i < 60 / slotLenth; i++) {
            
             slots.add("${twoDigitNumToString(count)}:${twoDigitNumToString(i * slotLenth)}");
      }      

      count++;
    } while (
      count <= closeHour
    );

    return slots;
  }
  

  String convertArmyTime(String time){
    int hr = textToHour(time);
    int mn = textToMin(time);
    String m = "00";
    if(mn != 0){
        m = mn.toString();
      }

    if(hr >=13){
      hr = hr -12;
      return "${hr}:${m}PM";
    }else{
      return "${hr}:${m}AM";

    }
  }
  String convertToClock(String time){
    List<String> t = time.split("T");
    List<String> tmp = t[1].split(":");
    int hr = int.parse(tmp[0]);
    int mn = int.parse(tmp[1]);
    String m = "00";
    if(mn != 0){
        m = mn.toString();
      }

    if(hr >= 13){
      hr = hr -12;
      return "${hr}:${m}PM";
    }else{
      return "${hr}:${m}AM";

    }
  }



  String formatTimeString(int hour, int min){
    return "${twoDigitNumToString(hour)}:${twoDigitNumToString(min)}";
  }

  String twoDigitNumToString(int number){
    String x = "";
    if(number < 10){
      x = "0${number}";
    } else {
      x = "${number}";
    }
    return x;
  }