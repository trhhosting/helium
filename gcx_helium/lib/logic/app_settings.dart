class AppSettings {
  String _appName;
  String _emailHash;
  int _daysSpan;
  String _lat;
  String _lon;
  String _address;
  String _appUrl;
  String _profileId;
  String _name;
  String _color;
  String _calendarId;

  AppSettings(
      {
        String appName,
        String emailHash,
        int daysSpan,
        String lat,
        String lon,
        String address,
        String appUrl,
        String profileId,
        String name,
        String color,
        String calendarId,

      }) {
    this._appName = appName;
    this._emailHash = emailHash;
    this._daysSpan = daysSpan;
    this._lat = lat;
    this._lon = lon;
    this._address = address;
    this._appUrl = appUrl;
    this._profileId =profileId;
    this._name =name;
    this._color =color;
    this._calendarId = calendarId;
  }

  String get appName => _appName;
  set appName(String appName) => _appName = appName;
  String get emailHash => _emailHash;
  set emailHash(String emailHash) => _emailHash = emailHash;
  int get daysSpan => _daysSpan;
  set daysSpan(int daysSpan) => _daysSpan = daysSpan;
  String get lat => _lat;
  set lat(String lat) => _lat = lat;
  String get lon => _lon;
  set lon(String lon) => _lon = lon;

  //address address of location
  String get address => this._address;
  set address(String address){
  this._address = address;
  }
  //appUrl Cronofy Application Url
  String get appUrl => this._appUrl;
  set appUrl(String appUrl){
  this._appUrl = appUrl;
  }

  //profileId Desc
  String get profileId => this._profileId;
  set profileId(String profileId){
  this._profileId = profileId;
  }

  //name Desc
  String get name => this._name;
  set name(String name){
  this._name = name;
  }

  //color Desc
  String get color => this._color;
  set color(String color){
  this._color = color;
  }
  //calendarId Desc
  String get calendarId => this._calendarId;
  set calendarId(String calendarId){
  this._calendarId = calendarId;
  }
    
  AppSettings.fromJson(Map<String, dynamic> json) {
    _appName = json['app_name'];
    _emailHash = json['email_hash'];
    _daysSpan = json['days_span'];
    _lat = json['lat'];
    _lon = json['lon'];
    _address = json['address'] as String;
    _appUrl = json['app_url'] as String;
    _profileId = json['profile_id'] as String;
    _name = json['name'] as String;
    _color = json['color'] as String;
    _calendarId = json['calendar_id'] as String;
    
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['app_name'] = this._appName;
    data['hash'] = this._emailHash;
    data['days_span'] = this._daysSpan;
    data['lat'] = this._lat;
    data['lon'] = this._lon;
    data['address'] = this._address;
    data['app_url'] = this._appUrl;
    data['profile_id'] = this._profileId;
    data['name'] = this._name;
    data['color'] = this._color;
    data['calendar_id'] = this._calendarId;
    return data;
  }
}
