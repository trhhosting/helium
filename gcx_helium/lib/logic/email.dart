class EmailSignUp {
  String _name;
  String _email;
  String _servicetype;
  String _phone;

  EmailSignUp({String name, String email, String servicetype, String phone}) {
    this._name = name;
    this._email = email;
    this._servicetype = servicetype;
    this._phone = phone;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get email => _email;
  set email(String email) => _email = email;
  String get servicetype => _servicetype;
  set servicetype(String servicetype) => _servicetype = servicetype;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;

  EmailSignUp.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _email = json['email'];
    _servicetype = json['servicetype'];
    _phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = this._name;
    data['email'] = this._email;
    data['servicetype'] = this._servicetype;
    data['phone'] = this._phone;
    return data;
  }
}