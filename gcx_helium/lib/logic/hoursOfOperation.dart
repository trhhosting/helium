class HoursOfOperation {
  int _id;
  String _day;
  String _openTime;
  String _closeTime;
  bool _opened;

  HoursOfOperation(
      {int id, String day, String openTime, String closeTime, bool opened}) {
    this._id = id;
    this._day = day;
    this._openTime = openTime;
    this._closeTime = closeTime;
    this._opened = opened;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get day => _day;
  set day(String day) => _day = day;
  String get openTime => _openTime;
  set openTime(String openTime) => _openTime = openTime;
  String get closeTime => _closeTime;
  set closeTime(String closeTime) => _closeTime = closeTime;
  bool get opened => _opened;
  set opened(bool opened) => _opened = opened;

  HoursOfOperation.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _day = json['day'];
    _openTime = json['open_time'];
    _closeTime = json['close_time'];
    _opened = json['opened'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = this._id;
    data['day'] = this._day;
    data['open_time'] = this._openTime;
    data['close_time'] = this._closeTime;
    data['opened'] = this._opened;
    return data;
  }
}
