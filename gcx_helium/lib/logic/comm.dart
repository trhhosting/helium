import 'package:http_client/browser.dart';
import 'package:http/http.dart' as http;
import 'dart:html';
import 'dart:convert';

import 'appointment.dart';
// import 'eventRequest.dart';

Future<String> dataServices() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/services.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> messageServices() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/message.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> timezones() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/timezones.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> hoursOfOperation() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/hoursofoperation.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> getCalendarDisplayData() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/calendar.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> getAppSettings() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/app.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> getHours() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/hours.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
void sendJsonStart(String data) async{
  String domain = "";
  domain = window.location.href;
  domain = domain.replaceAll(window.location.protocol, "");
  domain = domain.replaceAll("/", "");
  domain = domain.replaceAll("#", "_");
  var url = '/api/data/application/start/$domain';
  var headers = <String,String>{}; 
  headers = {'Content-Type':'application/json'};
  // await http.post(url,headers: headers, body: data);
  var response = await http.post(url,headers: headers, body: data);
  print(response.body);
}
void sendAuthCalendar(String data) async{
  String domain = "";
  domain = window.location.href;
  domain = domain.replaceAll(window.location.protocol, "");
  domain = domain.replaceAll("/", "");
  domain = domain.replaceAll("#", "_");
  var url = '/api/v1/calendar/auth/';
  var headers = <String,String>{}; 
  headers = {'Content-Type':'application/json'};
  // await http.post(url,headers: headers, body: data);
  var response = await http.post(url,headers: headers, body: data);
  print(response.body);
}
Future<String> getAvailability(dynamic data) async{
  print(data);
  var url = '/api/v1/calendar/schedule';
  var headers = <String,String>{}; 
  headers = {'Content-Type':'application/json'};
  // await http.post(url,headers: headers, body: data);
  var response = await http.post(url,headers: headers, body: data);
  // print(response.body);
  return response.body.toString();
}
void sendAppointmentJsonStart(TelMessage data) async{
  data.page = window.location.href; 
  var send = jsonEncode(data.toJson());
  var url = '/api/v1/calendar/start/';
  var headers = <String,String>{}; 
  headers = {'Content-Type':'application/json'};
  // await http.post(url,headers: headers, body: data);
  await http.post(url,headers: headers, body: send);
}
void sendAppointmentJsonEventSet(String data) async{
  var url = '/api/v1/calendar/appointment/';
  var headers = <String,String>{}; 
  headers = {'Content-Type':'application/json'};
  // await http.post(url,headers: headers, body: data);
  await http.post(url,headers: headers, body: data);
}
