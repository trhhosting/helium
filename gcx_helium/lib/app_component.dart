
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:logic/logic.dart';

import 'logic/comm.dart';

import 'src/routes.dart';
import 'src/shared/AppShell/app_shell.dart';

//Routing
@Component(
  selector: 'tlm-theme',
  templateUrl: 'app_component.html',
  styleUrls: [
    'app_component.css',
  ],
  directives: [
    routerDirectives,
    AppShell,
  ],
  exports: [
    RoutePaths,
    Routes,
  ],
)
class AppComponent implements OnInit, AfterViewInit{

  @override
  void ngOnInit() async{
    String displayText = await getCalendarDisplayData();
    await setData("calendar", displayText);

  }
  @override 
  void ngAfterViewInit() async{
    String appSettings = await getAppSettings();
    await setData("app", appSettings);
  }
}
