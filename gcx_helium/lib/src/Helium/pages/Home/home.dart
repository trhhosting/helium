import 'dart:async';
import 'dart:html';
import 'dart:convert';
import 'package:angular/angular.dart';
import '../../shared/Offer/offer.dart';
import '../../shared/OfferSignUp/offer_sign_up.dart';
import '../../shared/AppointmentSetup/appointment_setup.dart';
import 'package:logic/logic.dart';
import '../../../../logic/productData.dart';
import '../../../../logic/comm.dart';

@Component(
  selector: 'home',
  templateUrl: 'home.html',
  styleUrls: [
    "home.css",
    "intro.css",
    "responsive.css",
    "kaydee.css",
  ],
  directives: [
    coreDirectives,
    Offer,
    OfferSignUp,
    AppointmentSetup,
  ],
)
class Home implements OnInit, AfterChanges {
  @ViewChild('offdiv')
  Element offdiv;
  @ViewChild('emailstart')
  Element emailstart;
  @ViewChild('application')
  Element application;
  @ViewChild('calltoaction')
  Element calltoaction;
  @ViewChild('workflow')
  Element workflow;
  @ViewChild('emailstartbtn')
  AnchorElement emailstartbtn;
  @ViewChild('appointmentbtn')
  AnchorElement appointmentbtn;
  @ViewChild('addonscolor')
  DivElement addonscolor;
  @ViewChild('addon')
  DivElement addon;
  @ViewChild('wigmaintance')
  DivElement wigmaintance;
  @ViewChild('wiginstall')
  DivElement wiginstall;
  @ViewChild('starter')
  DivElement starter;
  @ViewChild('cartmessage')
  DivElement cartmessage;

  int screenWidth, screenHeight;
  String hash = "";
  String em;
  String serviesData = "";
  int forms = 0;
  int counter = 0;
  List<KayDeeServices> wigInstalls = <KayDeeServices>[];
  List<KayDeeServices> wigMaintenance = <KayDeeServices>[];
  List<KayDeeServices> sewIns = <KayDeeServices>[];
  List<KayDeeServices> addonColors = <KayDeeServices>[];
  List<KayDeeServices> addons = <KayDeeServices>[];
  List<KayDeeServices> services = <KayDeeServices>[];

  StreamController<dynamic> messageEvents = StreamController<dynamic>();

  @override
  void ngOnInit() async {
    top();
    hash = await getData("hash");
    // print(hash);
    em = await getData("email");

    await setData('addons', []);
    await setData('msg', await messageServices());
  // displayControl(counter);
  serviesData = await dataServices();
    await setData("services", serviesData);
    List<dynamic> serviceList = json.decode(serviesData);
    serviceList.forEach((dynamic d){
        var x = KayDeeServices.fromJson(d);
        if (x.category == "wig installs"){
          wigInstalls.add(x);
        }
        if (x.category == "addons color"){
          addonColors.add(x);
        }
        if (x.category == "addons"){
          addons.add(x);
        }
        if (x.category == "sewins"){
          sewIns.add(x);
        }
        if (x.category == "maintenance"){
          wigMaintenance.add(x);
        }
        services.add(x);
    });
    }
  
  @override 
  void ngAfterChanges() async{
      em = await getData("email");
      addtoCart();
  }
  
  void top(){
    window.scrollTo(0,0);
  }
  void book() async{
    workflow.hidden = false;
    calltoaction.hidden = true;
  }
  String selection = "";
  void select(String name) async {
    selection = name;
  }
  String addonSelection = "";
  void addonSelect(String name) async {
    selection = name;
  }
  List<String> addOnList = <String>[];
  var cartEvents = StreamController();

  void selectAddon(String addon) async{
    for(var i = 0; i < services.length; i++){
      if (services[i].name == addon){
        addOnList.add(addon);
        break;
      }
    }
    List<dynamic> x = <dynamic>[];
    x = await getData("addons");
    x.addAll(addOnList);
    await setData("addons",x);
    addOnList.clear();
    cartEvents.add(addon);
    print(addOnList);
    serviceCart();
  }
void emailStartBtn() async{
  var data = await messageServices();
  await setData("msg", data);
  selectAddon(selection);
  messageEvents.add(1);
  
  // inputform();

  displayControl(1);
  // print("email Start");
}
void applicationBtn(){
  print("application Start");

}
void clearCart() async{
  addOnList.clear();
  await setData("addons", []);
}
String addtoCart(){
  return "this is a test";
}
void prebtn(){
  counter--;
  if(counter < 0){
    counter = 0;
    // return;
  }
  print("prebtn: $counter");
  displayControl(counter);
}
void nxtbtn(){
  counter++;
  print("nxtbtn: $counter");
  displayControl(counter);
}
void serviceCart(){
  print("service-cart");
}
  void displayControl(int counter) async{
        if(counter == -1){

        }

        if (counter == 0){
          application.hidden = true;
          emailstart.hidden = false;
          addon.hidden = true;
          addonscolor.hidden = true;
          starter.hidden = false;
          cartmessage.hidden = true;
          workflow.hidden = false;
          calltoaction.hidden = true;
          wiginstall.hidden = false;
          wigmaintance.hidden = false;

        }
        if (counter == 1){
          starter.hidden = true;          
          application.hidden = false;
          emailstart.hidden = true;
          addon.hidden = false;
          addonscolor.hidden = false;
          wiginstall.hidden = true;
          wigmaintance.hidden = true;
          cartmessage.hidden = false;
        }
        if(counter == 2){
          addon.hidden = true;
          addonscolor.hidden = true;
        }
        if(counter == 3){

        }
  }

}
