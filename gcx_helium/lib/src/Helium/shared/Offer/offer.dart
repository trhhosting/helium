import 'package:angular/angular.dart';
import 'package:logic/logic.dart';
import 'dart:html';
import 'dart:async';
import 'dart:convert';
import '../../../../logic/productData.dart';
import '../../../../logic/items.dart';
import '../../../../logic/supportFunc.dart';
import '../../../../logic/message.dart';


@Component(
  selector: 'offer',
  templateUrl: 'offer.html',
  styleUrls: [
    "Features-Boxed.css",
    "offer.css"
  ],
  directives: [
    coreDirectives,
  ],
)
class Offer implements OnInit, AfterChanges {
  List<KayDeeServices> services = <KayDeeServices>[];
  
  @ViewChild('message')
  DivElement message;
  @ViewChild('display')
  DivElement display;
  @ViewChild('msg')
  ParagraphElement msg;

  String serviesData = "";
  Message msgData = Message();
  @Input('message-events')
  StreamController<dynamic> msgEvents = StreamController();

  @override 
  void ngOnInit() async{


  }

  @override 
  void ngAfterChanges() async{
      await msgEvents.stream.forEach((dynamic e){
        updateMessage();
      });

  }

  void updateMessage() async{
      display.hidden = true;
      message.hidden = false;
      var msg = await getData("msg");
      var dat = json.decode(msg);
      Message x = Message.fromJson(dat);
      msgData = x;

  }

}
Items selectItem(String name, List<KayDeeServices> services){
    var x = Items();
    for (var i = 0; i < services.length; i++) {
      if(services[i].name == name){
        var sr = services[i];
        x.desc = sr.description;
        x.name = sr.name;
        x.price = sr.price;
        x.id = i.toString();
        break;
      }
    }
    top();
    return x;
}
