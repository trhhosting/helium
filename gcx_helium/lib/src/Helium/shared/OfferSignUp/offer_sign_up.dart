import 'dart:html';
import 'dart:async';
import 'dart:convert';

import 'package:angular/angular.dart';

import 'package:http/http.dart' as http;
import 'package:logic/logic.dart';

import '../../../../logic/email.dart';
import '../../../../logic/productData.dart';
// import '../../../../logic/supportFunc.dart';
import '../../../../logic/app_settings.dart';
import '../../../../logic/appointment.dart';
// import 'dart:js' as js;
@Component(
  selector: 'offer-sign-up',
  templateUrl: 'offer_sign_up.html',
  styleUrls: [
    "offer_sign_up.css",
  ],
  directives: [
    coreDirectives,
  ]
)
class OfferSignUp implements OnInit, AfterChanges, AfterContentInit, AfterViewInit {
  @ViewChild('name')
  InputElement name;
  @ViewChild('email')
  InputElement email;
  @ViewChild('phone')
  InputElement phonenumber;
  @ViewChild('servicetype')
  SelectElement servicetype;
  @ViewChild('servicedesc')
  ParagraphElement servicedesc;
  @ViewChild('emailstartbtn')
  ButtonElement emailstartbtn;


  List<KayDeeServices> services = <KayDeeServices>[];
  List<KayDeeServices> wigInstalls = <KayDeeServices>[];
  List<KayDeeServices> wigMaintenance = <KayDeeServices>[];
  List<KayDeeServices> sewIns = <KayDeeServices>[];


  String serviesData = "";
  @Input()
  String selection = "";
  int counter = 1;
  StreamController<dynamic> cartStart = StreamController();

  @Output()
  Stream<dynamic> get sendIndex => cartStart.stream;

  
  AppSettings app = AppSettings();
  //sendIndex Desc

  bool start = false;

  @override
  void ngOnInit() async{
    print("Loding data ...");
    var i = 0;
    var ii = 0;
    do {
      if (i > 50){
        ii++;
      }
      if (ii > 3){
        break;
      }
      i++;
      serviesData = await getData('services');

      print("loading $i");
    } while (serviesData == null);
    List<dynamic> serviceList = json.decode(serviesData);
    serviceList.forEach((dynamic d){
        var x = KayDeeServices.fromJson(d);
        if (x.category == "sewins"){
          sewIns.add(x);
        }
        if (x.category == "maintenance"){
          wigMaintenance.add(x);
        }
        if (x.category == "wig installs"){
          wigInstalls.add(x);
        }
        services.add(x);

    });


    servicetype.onChange.listen((e) async{
      // print(servicetype.selectedIndex);
      // print(servicetype.value);
      for(var i = 0;i < services.length ; i++){
        if( services[i].name == servicetype.value){
          servicedesc.text = services[i].description;
          break;
        } 
      }
     // servicedesc.text = [servicetype.selectedIndex].description;
    });
    
    if(!start){
      name.value = nullValueInput(await getData("name"), name.value) ;
      email.value = nullValueInput(await getData("email"), email.value);
      phonenumber.value = nullValueInput(await getData("phone"), phonenumber.value);
      start = true;
    }


  }
  @override 
  void ngAfterChanges() async{
      // print("this is :${selection}");
      if (selection != null){
      servicetype.value = selection;
      for(var i = 0;i < services.length ; i++){
        if( services[i].name == servicetype.value){
          servicedesc.text = services[i].description;
        }
      }
    }

  }
  @override 
  void ngAfterContentInit() async{
    String a = "";
    int i = 0;
    do {
      a = await getData('app');
      if(a != null){
        break;
      }
      i++;
      print("loading ..$i.. $a appsettings");
    } while (i < 50);
    var aJson = json.decode(a);
    app = AppSettings.fromJson(aJson);
    print(app.appName);

  }
  @override 
  void ngAfterViewInit() async{
  }


  void save() async{
    // print('save');
    send();
  }
  EmailSignUp x = EmailSignUp();
  void send() async{
    // js.context.callMethod("run_locker");
    if (!validateEmailForm()){
      return;
    }
    cartStart.add("valid");

    x.email = nullValue(email.value);
    x.name = nullValue(name.value);
    x.phone = nullValue(phonenumber.value);
    x.servicetype = nullValue(servicetype.value);
    await setData("name", name.value);
    await setData("phone", phonenumber.value);
    await setData("email", email.value);
    await setData("servicetype", servicetype.value);
    //toDo enable this
    // TelMessage data = TelMessage(
    //   appName: app.appName,
    //   userHash: app.emailHash,
    //   name: x.name,
    //   phone: x.phone,
    //   email:  x.email,
    //   message: '''
    //     ${x.name} has added to cart ${x.servicetype}.
    //   ''',
    // );
 
    // sendJsonStart(data);
  }
  bool validateEmailForm(){
    if (!validateInputFieldFullName(name)){
      window.scrollTo(name.scrollTop,0);
      name.value = "";
      name.focus();
      return false;
    };
    if (!validateInputFieldEmail(email)){
      window.scrollTo(email.scrollTop,0);
      email.value = "";
      email.focus();
      return false;
    }
    if (!validateInputFieldPhone(phonenumber)){
      window.scrollTo(phonenumber.scrollTop,0);
      phonenumber.value = "";
      phonenumber.focus();
      return false;
    };
    if (servicetype.value == "Select Service"){
      window.scrollTo(servicetype.scrollTop,0);
      servicetype.focus();
      print("testServiceType");
      return false;
    };
    return true;
  }
  //TODO enable 
  void sendJsonStart(TelMessage data) async{
    data.page = window.location.href; 
    var send = jsonEncode(data.toJson());
    var url = '/api/v1/calendar/start/';
    var headers = <String,String>{}; 
    headers = {'Content-Type':'application/json'};
    // await http.post(url,headers: headers, body: data);
    await http.post(url,headers: headers, body: send);
  }
}

String nullValue(var data){
  if (data == null || data == 0 || data.toString().isEmpty){
    return 'none';
  }
  var x = data.toString();
  return x;
}
String nullValueInput(dynamic data, String value){
  if (data == null || data == 0 || data.toString().isEmpty){
    return value;
  }
  return data.toString();
}
