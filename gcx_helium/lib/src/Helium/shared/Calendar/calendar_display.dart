class CalendarDisplay {
  List<Weeks> _weeks;

  CalendarDisplay({List<Weeks> weeks}) {
    this._weeks = weeks;
  }

  List<Weeks> get weeks => _weeks;
  set weeks(List<Weeks> weeks) => _weeks = weeks;

  CalendarDisplay.fromJson(Map<String, dynamic> json) {
    if (json['weeks'] != null) {
      _weeks = <Weeks>[];
      json['weeks'].forEach((v) {
        _weeks.add(new Weeks.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this._weeks != null) {
      data['weeks'] = this._weeks.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Weeks {
  Day _day1;
  Day _day2;
  Day _day3;
  Day _day4;
  Day _day5;
  Day _day6;
  Day _day7;

  Weeks(
      {Day day1,
      Day day2,
      Day day3,
      Day day4,
      Day day5,
      Day day6,
      Day day7}) {
    this._day1 = day1;
    this._day2 = day2;
    this._day3 = day3;
    this._day4 = day4;
    this._day5 = day5;
    this._day6 = day6;
    this._day7 = day7;
  }

  Day get day1 => _day1;
  set day1(Day day1) => _day1 = day1;
  Day get day2 => _day2;
  set day2(Day day2) => _day2 = day2;
  Day get day3 => _day3;
  set day3(Day day3) => _day3 = day3;
  Day get day4 => _day4;
  set day4(Day day4) => _day4 = day4;
  Day get day5 => _day5;
  set day5(Day day5) => _day5 = day5;
  Day get day6 => _day6;
  set day6(Day day6) => _day6 = day6;
  Day get day7 => _day7;
  set day7(Day day7) => _day7 = day7;

  Weeks.fromJson(Map<String, dynamic> json) {
    _day1 = json['day1'] != null ? new Day.fromJson(json['day1']) : null;
    _day2 = json['day2'] != null ? new Day.fromJson(json['day2']) : null;
    _day3 = json['day3'] != null ? new Day.fromJson(json['day3']) : null;
    _day4 = json['day4'] != null ? new Day.fromJson(json['day4']) : null;
    _day5 = json['day5'] != null ? new Day.fromJson(json['day5']) : null;
    _day6 = json['day6'] != null ? new Day.fromJson(json['day6']) : null;
    _day7 = json['day7'] != null ? new Day.fromJson(json['day7']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this._day1 != null) {
      data['day1'] = this._day1.toJson();
    }
    if (this._day2 != null) {
      data['day2'] = this._day2.toJson();
    }
    if (this._day3 != null) {
      data['day3'] = this._day3.toJson();
    }
    if (this._day4 != null) {
      data['day4'] = this._day4.toJson();
    }
    if (this._day5 != null) {
      data['day5'] = this._day5.toJson();
    }
    if (this._day6 != null) {
      data['day6'] = this._day6.toJson();
    }
    if (this._day7 != null) {
      data['day7'] = this._day7.toJson();
    }
    return data;
  }
}

class Day {
  int _id;
  String _day;
  String _openTime;
  String _closeTime;
  bool _opened;
  String _date;
  int _dayNumber;
  List<String> _timeSlots;

  Day(
      {int id,
      String day,
      String openTime,
      String closeTime,
      bool opened,
      String date,
      int dayNumber,
      List<String> timeSlots}) {
    this._id = id;
    this._day = day;
    this._openTime = openTime;
    this._closeTime = closeTime;
    this._opened = opened;
    this._date = date;
    this._dayNumber = dayNumber;
    this._timeSlots = timeSlots;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get day => _day;
  set day(String day) => _day = day;
  String get openTime => _openTime;
  set openTime(String openTime) => _openTime = openTime;
  String get closeTime => _closeTime;
  set closeTime(String closeTime) => _closeTime = closeTime;
  bool get opened => _opened;
  set opened(bool opened) => _opened = opened;
  String get date => _date;
  set date(String date) => _date = date;
  int get dayNumber => _dayNumber;
  set dayNumber(int dayNumber) => _dayNumber = dayNumber;
  List<String> get timeSlots => _timeSlots;
  set timeSlots(List<String> timeSlots) => _timeSlots = timeSlots;

  Day.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _day = json['day'];
    _openTime = json['open_time'];
    _closeTime = json['close_time'];
    _opened = json['opened'];
    _date = json['date'];
    _dayNumber = json['day_number'];
    _timeSlots = json['time_slots'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = this._id;
    data['day'] = this._day;
    data['open_time'] = this._openTime;
    data['close_time'] = this._closeTime;
    data['opened'] = this._opened;
    data['date'] = this._date;
    data['day_number'] = this._dayNumber;
    data['time_slots'] = this._timeSlots;
    return data;
  }
}
