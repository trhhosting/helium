import 'dart:html';
import 'dart:async';
import 'dart:convert';
import 'package:angular/angular.dart';

import 'package:logic/logic.dart';
import 'displayText.dart';
import '../../../../logic/comm.dart';
import '../../../../logic/hoursOfOperation.dart';
import '../../../../logic/app_settings.dart';
import '../../../../logic/productData.dart';
import '../../../../logic/filledTimes.dart';
import '../../../../logic/supportFunc.dart';
import '../../../../logic/timeCompair.dart';

import '../../../../logic/eventRequest.dart';

import 'calendar_display.dart';
import 'appointment.dart';

@Component(
  selector: 'calendar',
  templateUrl: 'calendar.html',
  styleUrls: [
    // 'Ultimate-Event-Calendar.css',
    'calendar.css',
    'appointment_setup.css',
  ],
  directives: [
    coreDirectives,
  ],
)
class Calendar implements OnInit, AfterChanges, AfterViewInit, AfterContentInit {
  @ViewChild('daysofweek')
  DivElement daysofweek;
  @ViewChild('caldata')
  Element caldata;
  // UListElement caldata;
  // DivElement caldata;
  


  @Input('time-selection')
  DateTime timeselection = DateTime.now().add(Duration(days: 1));
  @Input('time-zone')
  String timezone = "EST";

  @Input('cart-items')
  List<String> cartList = <String>[];
  StreamController<dynamic> selectionEvents = StreamController();

  @Output('selction-index')
  Stream<dynamic> get selectionIndex => selectionEvents.stream;

  List<KayDeeServices> services = <KayDeeServices>[];
  List<KayDeeServices> cart = <KayDeeServices>[];

  List<HoursOfOperation> hours = <HoursOfOperation>[];
  String hoursData = "";
  
  CalendarDisplay calDsp = CalendarDisplay();
  String calendarDisplay = "";

  List<String> weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  List<String> months = ["January","February","March","April","May","June","July","August","September","October","November","December"];


  String serviceData = "";
  List<Weeks> weeks =<Weeks>[];
  List<Day> hour = <Day>[];
  Weeks week1 = Weeks();
  Weeks week2 = Weeks();
  Weeks week3 = Weeks();
  Weeks week4 = Weeks();
  Weeks week5 = Weeks();
  Weeks week6 = Weeks();

  FilledTimes timeUsed = FilledTimes();
  String seeTimes = "";
  String displayText = "";

  String monthTitle(){

  return '''
    ${months[timeselection.month -1]}
    ''';
  }
  String monthDay(){

    return '''
    ${weekdays[timeselection.weekday -1]}
    ''';
  }
  String monthTime(){

    return '''
    ${timeselection.toString()}
    ''';
  }
  @override 
  void ngOnInit() async{
    loadCalendarData(displayDefault);
    hoursData = await hoursOfOperation();
    List<dynamic> hoursOpen = json.decode(hoursData);
    for(var item in hoursOpen){
      HoursOfOperation x = HoursOfOperation.fromJson(item);
      hours.add(x);
    }
  }
  @override 
  void ngAfterChanges() async{

  }
  @override 
  void ngAfterViewInit() async{
    String hourText = await getHours();
    await setData("hour", hourText);
    List<dynamic> hourList = json.decode(hourText);
    
    hourList.forEach((dynamic d){
      var x = Day.fromJson(d);
      hour.add(x);
    });


    serviceData = await getData('services');
    // displayText = await getCalendarDisplayData();
    // await setData("calendar", displayText);

    displayText = await getCalendarDisplayData();
    try {
      List<dynamic> serviceList = json.decode(serviceData);
      serviceList.forEach((dynamic d){
          var x = KayDeeServices.fromJson(d);
          services.add(x);
      });
      
    } catch (e) {
      serviceData = "";
      do{
        serviceData = await getData('services');
        if(serviceData != null && serviceData != ""){
          break;
        }
        //print("loading...");
      } while (serviceData != "");
      List<dynamic> serviceList = json.decode(serviceData);
      serviceList.forEach((dynamic d){
          var x = KayDeeServices.fromJson(d);
          services.add(x);
      });

    }
    // seeTimes = await getBlockedTimes();
    // var bookedJson = json.decode(seeTimes);
    // timeUsed = FilledTimes.fromJson(bookedJson);
    // updateCalendarData(displayText);

    // await updateCalendar();

  }
  @override 
  void ngAfterContentInit() async{
   await updateCalendar();
  }
  void updateCalendar() async{
    displayText = await getCalendarDisplayData();
    // displayText = await getData('calendar');
    // print("displayText: $displayText");

    seeTimes = await getBlockedTimes();
    var bookedJson = json.decode(seeTimes);
    timeUsed = FilledTimes.fromJson(bookedJson);

    updateCalendarData(displayText);
  }

  //loadCalendarData load blank calender
  void loadCalendarData(String data) {
    var w = weeksMap(data);
    
    for (var i = 1; i <= w.length; i++ ){
      print(i);
      if(i == 1){
        week1 = w[i]; //updateCal(w[i], timeselection, 1);
      }
      if(i == 2){
        week2 = w[i];
      }
      if(i == 3){
        week3 = w[i];
      }
      if(i == 4){
        week4 = w[i];
      }
      if(i == 5){
        week5 = w[i];
      }
    }
  }
  int _daycount = 0;

  //updateCalendarData adds dates to clendar
  void updateCalendarData(String data) {
    var w = weeksMap(data);

    for (var i = 1; i <= w.length; i++ ){
      print(i);
      if(i == 1){
        // week1 = w[i]; //updateCal(w[i], timeselection, 1);
        week1 = updateCal(w[i], timeselection, 0);
      }
      if(i == 2){
        week2 = updateCal(w[i], timeselection, _daycount);;
      }
      if(i == 3){
        week3 = updateCal(w[i], timeselection, _daycount);
      }
      // if(i == 4){
      //   week4 = updateCal(w[i], timeselection, _daycount);
      // }
      // if(i == 5){
      //   week5 = updateCal(w[i], timeselection, _daycount);
      // }
    }
  }
  Weeks updateCal(Weeks wk, DateTime selected, int weekNo){
    // selected = DateTime(
    //  2020,
    //   11,
    //   12 
    // );
    
    List<Day> week = <Day>[];
    for (var d in weekdays){
      Day dx = Day(
        day: d,
        dayNumber: 0,
      );
      week.add(
        dx,
      );
    }
    Weeks w = Weeks(
      day1: week[0],
      day2: week[1],
      day3: week[2],
      day4: week[3],
      day5: week[4],
      day6: week[5],
      day7: week[6],
    );

    int i = 0;
    selected = selected.add(Duration(days: weekNo));
    switch (selected.weekday) {
      case 1:
        w.day1 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );
          w.day2 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );
          w.day3 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day4 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day5 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day6 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day7 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );          
        break;
      case 2:
          w.day2 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );
          w.day3 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day4 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day5 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day6 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day7 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );
        break;
      case 3:
          w.day3 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day4 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day5 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day6 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day7 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );
        break;
      case 4:
          w.day4 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day5 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day6 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day7 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );
        break;
      case 5:
          w.day5 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day6 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day7 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );
        break;
      case 6:
          w.day6 = updateDay(
                      selected.add(Duration(days: i++)),
                      [
                        "11:00am",
                        " 1:00pm",
                        " 3:00pm",
                      ],
                      "10:00",
                      "20:00",
                      true,
                    );
          w.day7 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );
        break;
      case 7:
          w.day7 = updateDay(
                      selected.add(Duration(days: i++)),
                      [],
                      "10:00",
                      "20:00",
                      false,
                    );
        break;
      default:
    }
    _daycount = _daycount + i;

    w.day1.timeSlots = hour[0].timeSlots;
    w.day2.timeSlots = hour[1].timeSlots;
    w.day3.timeSlots = hour[2].timeSlots;
    w.day4.timeSlots = hour[3].timeSlots;
    w.day5.timeSlots = hour[4].timeSlots;
    w.day6.timeSlots = hour[5].timeSlots;
    w.day7.timeSlots = hour[6].timeSlots;
    if(timeUsed.blockedTimes != null){
      w.day1.timeSlots = compairTimeDates(w.day1, timeUsed.blockedTimes);
      w.day2.timeSlots = compairTimeDates(w.day2, timeUsed.blockedTimes);
      w.day3.timeSlots = compairTimeDates(w.day3, timeUsed.blockedTimes);
      w.day4.timeSlots = compairTimeDates(w.day4, timeUsed.blockedTimes);
      w.day5.timeSlots = compairTimeDates(w.day5, timeUsed.blockedTimes);
      w.day6.timeSlots = compairTimeDates(w.day6, timeUsed.blockedTimes);
      w.day7.timeSlots = compairTimeDates(w.day7, timeUsed.blockedTimes);
    }

    return w;
  }

  List<String> compairTimeDates(Day day, List<BlockedTimes> blocked){
      if(day.date == null){
        print("compairTimeDates null");
        return[];
      }
      if(!day.opened){
        print("compairTimeDates closed");
        return ["closed"];
      }

      List<DateRangeSlot> filledTimes = <DateRangeSlot>[];

      print(day.date);
      print(day.openTime);
      // 2020-11-25T08:57:00Z
      DateTime open = DateTime(
        textToYear(day.date),
        textToMonth(day.date),
        textToDay(day.date),
        textToHour(day.openTime),
        textToMin(day.openTime),
      );
      DateTime close = DateTime(
        textToYear(day.date),
        textToMonth(day.date),
        textToDay(day.date),
        textToHour(day.closeTime),
        textToMin(day.closeTime),
      );
      print("Dpen: ${open}");
      print("Close: ${close}");

      for (var block in blocked){
        DateRangeSlot x = DateRangeSlot();
        x.start = DateTime.parse(block.startDate).toLocal();
        x.end = DateTime.parse(block.endDate).toLocal();
        if(
          x.start.microsecondsSinceEpoch > close.microsecondsSinceEpoch
          ||
          x.end.microsecondsSinceEpoch < open.microsecondsSinceEpoch
          ){
          continue;
        }
        filledTimes.add(x);
        // print(x.start);
        // print(x.end);
      }
      int timeslotLenth = 30;
      //full day
      // List<String> sltDay = timeSlots("00:00", "24:00", timeslotLenth);
      // print(sltDay);


      List<String> slt = timeSlots(day.openTime, day.closeTime, timeslotLenth);

      //todo finish removing the slots with compair logic
      List<String> removal = <String>[];

      for (var b in filledTimes){
        TimeCompair start = TimeCompair.fromDateTime(b.start);
        TimeCompair end = TimeCompair.fromDateTime(b.end);

        List<String> rm = timeSlots(
          formatTimeString(start.hour, start.min), 
          formatTimeString(end.hour, end.min), 
          timeslotLenth,
          );
        for(var i in rm){
          removal.add(i);
        }

        // print('''
        // #Look inside loop
        // ${start.hour} ${start.min}
        // ${end.hour} ${end.hour}
        // ${rm}
        // ''');
          
          
        }
        
        for(var r in removal){
          slt.remove(r); 
        }
        // print("sltDay Removal : ${removal}");
        // print("sltDay Removal : ${slt}");

        List<String> avail = <String>[];
        for(var i in slt){
          avail.add(armyTime(i));
        }

      // print("Open Time: ${day.openTime}");
      // print("Close Time: ${day.closeTime}");
    return avail;

  }
  //weeksMap Loads default map data with all turned off 
  Map<int,Weeks> weeksMap(String data) {
     Map<int,Weeks> da = <int,Weeks>{};
    dynamic jsonData = json.decode(data);
    CalendarDisplay dsp = CalendarDisplay.fromJson(jsonData);
    int i = 0;
    for(var item in dsp.weeks){
      i++;
      da[i] = item;
    }
    return da;
  
  }
  String armyTime(String time){
    int hr = textToHour(time);
    int mn = textToMin(time);
    String m = "00";
    if(mn != 0){
        m = mn.toString();
      }

    if(hr >= 13){
      hr = hr -12;
      return "${hr}:${m}:PM";
    }else{
      return "${hr}:${m}:AM";

    }
  }
  Future<List<KayDeeServices>> cartItems() async{
    List<KayDeeServices> x = <KayDeeServices>[];
    List<String> items = <String>[];
    List<dynamic> addons = await getData("addons");
    dynamic servicetype = await getData("servicetype");
    addons.forEach((dynamic d){
      items.add(d.toString());
    });
    items.add(servicetype.toString());

    for(var i in services){
      for(var it in items){
        if(it == i.name){
          x.add(i);
        }
      }
    }
    return x;
  }


  Future<String> getBlockedTimes() async{
    String app = await getData('app');
    dynamic jsonData = json.decode(app);
    AppSettings aps = AppSettings.fromJson(jsonData);
    String service = await getData("servicetype");
    List<dynamic> addons = await getData("addons");
    int before, after, duration = 0;

    for (var i in services){
      for(var x in addons){
        if(i.name == x){
          duration = duration + i.duration;
        }
      }
      if(i.name == service){
        before = i.timeBlock.before;
        after = i.timeBlock.after;
        duration = duration + i.duration;

      }
    }

    String appointmentRequest = '''
      {
          "appname": "${aps.appName}",
          "time_block": {
              "before": ${before},
              "after": ${after},
              "duration": ${duration},
              "date_time": "${timeselection.toIso8601String()}",
              "time_zone": "${timeselection.timeZoneName}",
              "time_open": "10:00",
              "time_close": "20:00"
          },
          "hash": "${aps.emailHash}",
          "client_name": "${await getData("name")}",
          "client_email": "${await getData("email")}",
          "client_number": "${await getData("phone")}",
          "days_span": ${aps.daysSpan}
      }
      ''';


      
    return getAvailability(appointmentRequest);
  }


  void appointementSelection(String date, String time) async{
    selectDate(date, time);
    print("date: ${date} time: ${time}");
  }
  void appointementSelectionButton(String date, String selectId) async{
    String id = selectId;

    SelectElement select = new SelectElement();
    select = querySelector(id);
    selectDate(date, select.value);
    // print("date: ${date} time: ${select.value} ${id}");
  }

  bool dataloaded = false;
  AppSettings appSettings = AppSettings();
  String email, name, phone = "";
  
  void selectDate(String date, String time) async{
        // 10:00AM or 1:00AM
    List<String> tt = time.split(":");
    int hour, min = 0;
    if(tt[2] == "AM"){
        hour = int.parse(tt[0]);
    }
    if(tt[2] == "PM"){
       hour = int.parse(tt[0]) + 12; 
    }
    min = int.parse(tt[1]);

    DateTime dt = DateTime(
      textToYear(date),
      textToMonth(date),
      textToDay(date),
      hour,
      min,
    );
    
    if(!dataloaded){
      String app = await getData('app');
      print(app);
      dynamic jsonData = json.decode(app);
      appSettings = AppSettings.fromJson(jsonData);
      email = await getData('email');
      name = await getData('name');
      phone = await getData('phone');
      dataloaded = true;

    }
    Location local = Location(
      description: appSettings.address,
      lat: appSettings.lat,
      long: appSettings.lon,
    );
    List<Reminders> reminders = <Reminders>[];
    //todo set reminder interval
    List<int> remind = <int>[15, 45, 120];
    for(var v in remind){
      Reminders x = Reminders();
      x.minutes = v;
      reminders.add(x);
    }
    InviteAtt addCustomer = InviteAtt();
    addCustomer.email = email;
    addCustomer.displayName = name;
    Attendees attendees = Attendees();
    attendees.inviteAtt = [addCustomer];

    // iso8601ToRFC3339(dt)
    // print(dt.toIso8601String());
    Start startTime = Start();
    startTime.time = iso8601ToRFC3339(dt);
    startTime.tzid = dt.timeZoneName;
    // print(startTime.time);
    int lenth = 0;
    int price = 0;

    for(var i in services){
      for(var v in cartList){
        if(v == i.name){
            lenth = lenth + i.duration + i.timeBlock.after + i.timeBlock.before ;
            // print("lenth: ${lenth}");
            price = price + i.price;
            // print('price: ${i.price}');
            // print("Price: ${price}");

        }
      }
    }
    await setData('appointmentStart', "${dt.toIso8601String()}");
    dt = dt.add(Duration(minutes: lenth));
    await setData('appointmentEnd', "${dt.toIso8601String()}");
    // print(dt.toIso8601String());
    Start endTime = Start();
    endTime.time = iso8601ToRFC3339(dt);
    // endTime.time = iso8601ToRFC3339(dt);
    endTime.tzid = dt.timeZoneName;
    // print(endTime.time);

    Event event = Event();
    event.endees = attendees;
    event.description = 
        "${name} booked for ${cartList[0]} with ${cartList.length -1} addons. Time min: ${lenth}";
    event.start =  startTime;
    event.end = endTime;
    event.eventId = DateTime.now().microsecondsSinceEpoch.toString();
    event.location = local;
    event.reminders = reminders;
    event.summary = '''
    ${name} booked an appointment with total 
      value: ${price}
      minus deposit made.
    ''';
    event.url = appSettings.appUrl;
    
    print("appsettings ${appSettings.calendarId}");
    EventRequest req = EventRequest();
    req.email =  appSettings.emailHash;
    req.emailHash = appSettings.profileId;
    req.appName = appSettings.appName;
    print(appSettings.profileId);
    req.client = Client(
      email: email,
      phoneNumber: phone,
      name: name,
    );
    req.event = event;
    req.page = window.location.href;
    req.calendarId = appSettings.calendarId;

    var te = req.toJson();
    String teJson = json.encode(te);
    print(teJson);
    await setData('appointment', teJson);

    

  }
  
}

Map<int,int> calNumDays = {
  1:31,
  2:28,
  3:31,
  4:30,
  5:31,
  6:30,
  7:31,
  8:31,
  9:30,
  10:31,
  11:30,
  12:31,
  }; 

Day updateDay(DateTime dateTime, List<String> timeSlots, [String openTime, String closeTime, bool opened=false]  ){
  List<String> weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

  String day = '''
  {
    "id": ${dateTime.weekday},
    "day": "${weekdays[dateTime.weekday -1]}",
    "open_time": "${openTime}",
    "close_time": "${closeTime}",
    "opened": ${opened},
    "date": "${iso8601ToRFC3339(dateTime)}",
    "day_number": ${dateTime.day},
    "time_slots": [
        
    ]
  }
  ''';
  var dayJson = json.decode(day);
  Day d = Day.fromJson(dayJson);
  d.timeSlots = timeSlots;
  return d;


}



  