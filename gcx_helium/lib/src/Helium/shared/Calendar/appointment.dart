class DateRangeSlot {
DateTime _start;
DateTime _end;

DateRangeSlot();
DateRangeSlot.Manual(DateTime start, DateTime end){
  this._end = end;
  this._start = start;
}

//start start of appointment
DateTime get start => this._start;
set start(DateTime start){
this._start = start;
}

//end end of the appointment
DateTime get end => this._end;
set end(DateTime end){
this._end = end;
}

}