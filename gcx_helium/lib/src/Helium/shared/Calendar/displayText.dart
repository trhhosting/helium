String displayDefault = '''
{
    "weeks": [
        {
            "day1": {
                "id": 1,
                "day": "Monday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day2": {
                "id": 2,
                "day": "Tuesday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day3": {
                "id": 3,
                "day": "Wednesday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "11:00am",
                    "1:00pm",
                    "3:00pm"
                ]
            },
            "day4": {
                "id": 4,
                "day": "Thursday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day5": {
                "id": 5,
                "day": "Friday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day6": {
                "id": 6,
                "day": "Saturday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day7": {
                "id": 7,
                "day": "Sunday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            }
        },
        {
            "day1": {
                "id": 1,
                "day": "Monday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day2": {
                "id": 2,
                "day": "Tuesday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day3": {
                "id": 3,
                "day": "Wednesday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "11:00am",
                    "1:00pm",
                    "3:00pm"
                ]
            },
            "day4": {
                "id": 4,
                "day": "Thursday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day5": {
                "id": 5,
                "day": "Friday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day6": {
                "id": 6,
                "day": "Saturday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day7": {
                "id": 7,
                "day": "Sunday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            }
        },
        {
            "day1": {
                "id": 1,
                "day": "Monday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day2": {
                "id": 2,
                "day": "Tuesday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day3": {
                "id": 3,
                "day": "Wednesday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "11:00am",
                    "1:00pm",
                    "3:00pm"
                ]
            },
            "day4": {
                "id": 4,
                "day": "Thursday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day5": {
                "id": 5,
                "day": "Friday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day6": {
                "id": 6,
                "day": "Saturday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day7": {
                "id": 7,
                "day": "Sunday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            }
        },
        {
            "day1": {
                "id": 1,
                "day": "Monday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day2": {
                "id": 2,
                "day": "Tuesday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day3": {
                "id": 3,
                "day": "Wednesday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "11:00am",
                    "1:00pm",
                    "3:00pm"
                ]
            },
            "day4": {
                "id": 4,
                "day": "Thursday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day5": {
                "id": 5,
                "day": "Friday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day6": {
                "id": 6,
                "day": "Saturday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day7": {
                "id": 7,
                "day": "Sunday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            }
        },
        {
            "day1": {
                "id": 1,
                "day": "Monday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day2": {
                "id": 2,
                "day": "Tuesday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day3": {
                "id": 3,
                "day": "Wednesday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "11:00am",
                    "1:00pm",
                    "3:00pm"
                ]
            },
            "day4": {
                "id": 4,
                "day": "Thursday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day5": {
                "id": 5,
                "day": "Friday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "10:00am",
                    " 1:00pm",
                    " 3:00pm",
                    " 5:30pm"
                ]
            },
            "day6": {
                "id": 6,
                "day": "Saturday",
                "open_time": "10:00",
                "close_time": "20:00",
                "opened": true,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            },
            "day7": {
                "id": 7,
                "day": "Sunday",
                "open_time": "",
                "close_time": "",
                "opened": false,
                "date": "",
                "day_number": 0,
                "time_slots": [
                    "closed"
                ]
            }
        }
    ]
}
''';
