import 'dart:async';

import 'package:angular/angular.dart';
import 'package:app/logic/comm.dart';
import 'dart:html';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:logic/logic.dart';
import '../../../../logic/productData.dart';
import '../../../../logic/supportFunc.dart';
import '../../../../logic/timeZone.dart';
import '../../../../logic/filledTimes.dart';
import '../Calendar/calendar.dart';

@Component(
  selector: 'appointment-setup',
  templateUrl: 'appointment_setup.html',
  styleUrls: [
    'appointment_setup.css',
  ],
  directives: [
    coreDirectives,
    Calendar,
  ],
)
class AppointmentSetup  implements OnInit, AfterChanges, AfterViewInit  {
  //UI Elements For the work application
  @ViewChild('addons')
  DivElement addons;
  @ViewChild('appointment')
  DivElement appointment;
  @ViewChild('cartdisplay')
  DivElement cartdisplay;
  @ViewChild('payment')
  DivElement payment;
  @ViewChild('availability')
  DivElement availability;
  @ViewChild('submitnav')
  DivElement submitnav;
 
  @ViewChild('name')
  HeadingElement name;
  @ViewChild('email')
  HeadingElement email;

  @ViewChild('msg')
  ParagraphElement msg;

  @ViewChild('servicetype')
  SelectElement servicetype;

  @ViewChild('selectzone')
  SelectElement selectzone;


  String userhash;
  int appSection = 0;
  bool datasent = false;
  bool emailCompleted = false;
  //todo change for buisness model
  int deposit = 40;


  @Input('cart-events')
  StreamController<dynamic> cartEvents = StreamController();

  @ViewChild('prebtn')
  ButtonElement prebtn;
  @ViewChild('nxtbtn')
  ButtonElement nxtbtn;

  StreamController preEvents = StreamController();
  StreamController nxtEvents = StreamController();

  @Output('back')
  Stream<dynamic> get backbtn => preEvents.stream;

  @Output('next')
  Stream<dynamic> get fowardbtn => nxtEvents.stream;

  
  List<KayDeeServices> addonColor = <KayDeeServices>[];
  List<KayDeeServices> addon = <KayDeeServices>[];
  List<KayDeeServices> service = <KayDeeServices>[];
  String serviesData = "";
  List<String> cartList = <String>[];
  List<KayDeeServices> cart = <KayDeeServices>[];

  List<TimeZone> timeZones = <TimeZone>[];
  int totalPrice = 0;
  int counter = 0;
  @override
  void ngOnInit() async{
    userhash = await getData('hash');
    name.text = await getData('name');
    email.text = await getData('email');
    serviesData = await loadServices();
    List<dynamic> serviceList = json.decode(serviesData);
    serviceList.forEach((dynamic d){
    var x = KayDeeServices.fromJson(d);
        if (x.category == "addons"){
          addon.add(x);
        }
        if (x.category == "addons color"){
          addonColor.add(x);
        }
    service.add(x);
    });
    selectzone.onFocus.listen((e) async{
        String tmz = "";
        try {
        tmz = await getData('tmz');
        if(tmz == null){
          tmz = await setData("tmz", await timezones());
          print('load tmz ...');
        };
        } catch (e) {
          print(e);
        }
        timeZones.clear();
        List<dynamic> tmzJson = json.decode(tmz);
        for (var item in tmzJson){
          TimeZone tz = TimeZone.fromJson(item);
          timeZones.add(tz);
        }
    });
    displayControl(counter);
    await cartEvents.stream.forEach((dynamic e){
      updateCart();
    });

  }
  void updateCart() async{
      List<dynamic> s = <dynamic>[];
      String item = "";
       try {
          s = await getData("addons");
          item = await getData('servicetype');
        } catch (e) {
          s = null;
        }
        cartList.clear();
        cartList.add(item);
        if (s != null){
          s.forEach((dynamic e){
            if(!cartList.contains(e.toString())){
              cartList.add(e.toString());
            }
          });
        }
        cart = displayCart();
  }
  List<KayDeeServices> displayCart(){
    List<KayDeeServices> c = <KayDeeServices>[];
      for (var item in service) {
        for (var v in cartList) {
          if(item.name == v.toString()){
            c.add(item);
            continue;
          }
        }
      }
      return c;
  }
  void addToCart(String item) async{
        if(item.isEmpty){
          return;
        }
      List<dynamic> s = <dynamic>[];
        try {
          s = await getData("addons");
        } catch (e) {
          s = null;
        }
        if (s == null){
          return;
        }
        s.add(item);

        await setData("addons",s);
        if (s != null){
          s.forEach((dynamic e){
            if(!cartList.contains(e.toString())){
              cartList.add(e.toString());
            }
          });
        }
      cart = displayCart();
  }
  @override 
  void ngAfterViewInit() async {
    await servicetype.onClick.forEach((dynamic e) {
      if (servicetype.value != "Select Addon"){
        addToCart(servicetype.value);
      }
      if (servicetype.value == "Select Addon"){
        updateCart();
      }
    });

  }
  @override 
  void ngAfterChanges() async{
    updateCart();
    // print("the cartEvents $cartEvents");
  }

  void prePart() async{
    counter--;
    if(counter < 0){
      counter = 0;
    }
    preEvents.add("prePart");
    displayControl(counter);
    print(counter);
  }

  void nxtPart() async{
    counter++;
    if(counter > 3){
      counter = 3;
    }
    nxtEvents.add("nxtPart");
    displayControl(counter);
    print(counter);
  }
  void save() async{

  }
  void delFromCart(String item) async{
      if(item.isEmpty){
          return;
      }
      List<dynamic> s = <dynamic>[];
      try {
        s = await getData("addons");
      } catch (e) {
        s = null;
      }
      s.remove(item);
      await setData("addons",s);
      await updateCart();
  }

  void delCartItem(KayDeeServices item) async{
    cartList.remove(item.name);
    cart.remove(item);
    // await setData("addons",cartList);
    delFromCart(item.name);
    print(item.name);
  }
  int total(){
    int p = 0;
    for (var item in cart){
      p = p + item.price;
    }
    return p;
  }
  double tax(){
    return  num.parse((total() * .0625).toStringAsFixed(2));
  }
  
  FilledTimes availabeDates(){

    FilledTimes x = FilledTimes();
    return x;
  }
  void sendJson(String data) async{
    String domain = "";
    domain = window.location.href;
    domain = domain.replaceAll(window.location.protocol, "");
    domain = domain.replaceAll("/", "");
    domain = domain.replaceAll("#", "_");
    await setData("name", name.text.trimRight());
    var url = '/api/data/application/position/$domain';
    var headers = <String,String>{}; 
    headers = {'Content-Type':'application/json'};
    await http.post(url,headers: headers, body: data);
    // var response = await http.post(url,headers: headers, body: data);
    // print(response.body);
    }
  //todo add messages for form
  Map<int,dynamic> messages = <int, dynamic>{
    0: "",
    1: "",
    2: "",
    3: "",
    4: "",
  };
  Map<int,dynamic> titles = <int, dynamic>{
    0: "",
    1: "",
    2: "",
    3: "",
    4: "",
  };
  void displayControl(int counter){
    // print("CounterDisplay $counter");
    if(counter == 0){
      addons.hidden = false;
      appointment.hidden = true;
      cartdisplay.hidden = false;
      payment.hidden = true;
      availability.hidden = true;
      
    }
    if(counter == 1){
      addons.hidden = true;
      appointment.hidden = false; 
      cartdisplay.hidden = true;
      payment.hidden = true;
    availability.hidden = true;

    }
    if(counter == 2){
      addons.hidden = true;
      appointment.hidden = true;
      cartdisplay.hidden = false;
      payment.hidden = true;
      availability.hidden = false;
      displayAppointment();
    }
    if(counter == 3){
      addons.hidden = true;
      appointment.hidden = true;
      cartdisplay.hidden = true;
      payment.hidden = false;
      availability.hidden = true;
      submitnav.hidden = true;

    }
  
  }
  String appointmentTime = "";
  String appointmentDate = "";
  void displayAppointment() async{
    String start = await getData('appointmentStart');
    appointmentDate = "${textToMonth(start)}/${textToDay(start)}/${textToYear(start)}";
    appointmentTime = "${convertToClock(start)}";
  }
  bool booked = false;
  void bookTime() async {
    if(booked){
      return;
    } else{
      booked = true;
    }
    var tmp = await getData('appointment');
    // print(tmp);
    
    await sendAppointmentJsonEventSet(tmp);
    AnchorElement an = AnchorElement();
    an.setAttribute("href", "/#/received-application");
    an.click();
    window.scrollTo(0,0);
  }
  void paymentBtn(){

  }
}

